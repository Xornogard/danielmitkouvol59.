﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public static CameraFollow instance;
    public bool CanFollow;
    public Transform targetTransform;
    public float fLerpSpeed;
    private Vector3 startPos;
    bool bFollowTarget;
	// Use this for initialization
	void Awake () {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        startPos = transform.position;
    }
	

    public void ReturnToStartPosition()
    {
        transform.position = startPos;
    }

    public void SetFollowTarget(bool _bFollowTarget)
    {
        bFollowTarget = _bFollowTarget;
    }
	// Update is called once per frame
	void FixedUpdate () {

        if (bFollowTarget == false) return;

        Vector3 targetPos = targetTransform.position;
        targetPos.z = transform.position.z;

        transform.position = Vector3.Lerp(transform.position, targetPos, Time.fixedDeltaTime * fLerpSpeed);
	}
}
