﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class SpriteSpreadingTool : MonoBehaviour {

    public float fMarginLeft;
    public Transform spritesParentTransform;

    public void SpreadSpritesBasedOnMarginValue()
    {
        Vector3 currentSpritePosition = spritesParentTransform.GetChild(0).position;

        for (int i = 1; i < spritesParentTransform.childCount; i++)
        {
            currentSpritePosition += Vector3.right * fMarginLeft;
            spritesParentTransform.GetChild(i).position = currentSpritePosition;
        }
    }
}

[CustomEditor(typeof(SpriteSpreadingTool))]
public class SpriteSpreadingToolEditor: Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SpriteSpreadingTool spriteSpreadingTool = (SpriteSpreadingTool)target;

        if (GUILayout.Button("Spread Sprites"))
        {
            spriteSpreadingTool.SpreadSpritesBasedOnMarginValue();
        }
    }
}
