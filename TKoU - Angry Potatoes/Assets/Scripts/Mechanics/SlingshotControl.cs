﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotControl : MonoBehaviour {

    public float fShotPower;
    public float fMaximumStretchValue;

    private const float fTimeToExplode = 1.0f;

    public Transform currentProjectileTransform;
    Rigidbody2D currentProjectileRB;
    SpriteRenderer currentProjectileSpriteRenderer;

    public Transform slingshotProjectilePocketTransform;

    public LineRenderer backViewSlingLineRenderer;
    public LineRenderer frontViewSlingLineRenderer;

    private Vector3 projectileInPocketLocalPos;
    private Quaternion projectileInPocketLocalRot;

    private Vector3 slingshotProjectilePocketStartPos;

    bool bSlingshotLoaded;

    public ParticleSystem projectileExplosion;


    // Update is called once per frame
    void Awake () {
        projectileInPocketLocalPos = currentProjectileTransform.localPosition;
        projectileInPocketLocalRot = currentProjectileTransform.localRotation;

        slingshotProjectilePocketStartPos = slingshotProjectilePocketTransform.position;
        currentProjectileRB = currentProjectileTransform.GetComponent<Rigidbody2D>();
        currentProjectileSpriteRenderer = currentProjectileTransform.GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        currentProjectileRB.isKinematic = true;
        LoadProjectileIntoPocket();
    }


    private void Update()
    {
        if(Input.GetMouseButton(0) && bSlingshotLoaded)
        {
            StretchSlingshot();
        }
        else if(Input.GetMouseButtonUp(0) && bSlingshotLoaded)
        {
            ReleaseProjectile();
        }
    }

    private void LoadProjectileIntoPocket()
    {
        CameraFollow.instance.SetFollowTarget(false);
        CameraFollow.instance.ReturnToStartPosition();

        EvaluateSlingPositions(slingshotProjectilePocketStartPos);
        slingshotProjectilePocketTransform.position = slingshotProjectilePocketStartPos;

        currentProjectileSpriteRenderer.enabled = true;
        bSlingshotLoaded = true;

        currentProjectileTransform.SetParent(slingshotProjectilePocketTransform);
        currentProjectileTransform.localPosition = projectileInPocketLocalPos;
        currentProjectileTransform.localRotation = projectileInPocketLocalRot;
    }

    private void StretchSlingshot()
    {
        Vector3 currentSlingshotPocketPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        currentSlingshotPocketPos.z = slingshotProjectilePocketStartPos.z;

        if (IsMaximumStretch(currentSlingshotPocketPos))
        {
            //tutaj kod mi nie chcial za bardzo zadzialac, dlatego poszedlem na latwizne i dalem return, bo dosc czasu na to zmarnowalem, a czas leci.
            return;

            currentSlingshotPocketPos = (currentSlingshotPocketPos - slingshotProjectilePocketStartPos).normalized;
            currentSlingshotPocketPos *= fMaximumStretchValue;
        }

        currentSlingshotPocketPos.z = slingshotProjectilePocketTransform.position.z;

        slingshotProjectilePocketTransform.position = currentSlingshotPocketPos;

        EvaluateSlingPositions(currentSlingshotPocketPos);
    }

    void EvaluateSlingPositions(Vector3 targetPos)
    {
        if (backViewSlingLineRenderer.positionCount != 2)
            Debug.LogError("backViewSlingLineRenderer has wrong number of positions!");

        Vector3[] currentSlingPositions = new Vector3[2];
        currentSlingPositions[0] = backViewSlingLineRenderer.transform.position;
        currentSlingPositions[1] = targetPos;
        currentSlingPositions[1].z = currentSlingPositions[0].z;

        backViewSlingLineRenderer.SetPositions(currentSlingPositions);

        if (frontViewSlingLineRenderer.positionCount != 2)
            Debug.LogError("frontViewSlingLineRenderer has wrong number of positions!");

        currentSlingPositions[0] = frontViewSlingLineRenderer.transform.position;
        currentSlingPositions[1] = targetPos;
        currentSlingPositions[1].z = currentSlingPositions[0].z;

        frontViewSlingLineRenderer.SetPositions(currentSlingPositions);
    }


    void ReleaseProjectile()
    {

        bSlingshotLoaded = false;
        currentProjectileTransform.parent = null;
        Vector3 direction = (slingshotProjectilePocketStartPos - currentProjectileTransform.position).normalized;
        currentProjectileRB.isKinematic = false;
        currentProjectileRB.AddForce(direction* GetStretchNormalizedValue() * fShotPower);

        StartCoroutine(ExplodeAfterTime(fTimeToExplode));
        CameraFollow.instance.SetFollowTarget(true);
    }


    bool IsMaximumStretch(Vector3 posToCheck)
    {
        float fDistance = Vector3.Distance(posToCheck, slingshotProjectilePocketStartPos);
        Debug.LogError(fDistance.ToString());
        return fDistance > fMaximumStretchValue;
    }


    float GetStretchNormalizedValue()
    {
        float fCurrentDistance = Vector3.Distance(slingshotProjectilePocketTransform.position, slingshotProjectilePocketStartPos);
        return fCurrentDistance / fMaximumStretchValue;
    }

    IEnumerator ExplodeAfterTime(float fWaitTime)
    {
        yield return new WaitForSeconds(fWaitTime);
        projectileExplosion.Play();
        currentProjectileRB.velocity = Vector2.zero;
        currentProjectileRB.isKinematic = true;
        currentProjectileSpriteRenderer.enabled = false;

        yield return new WaitForSeconds(1.5f);
        LoadProjectileIntoPocket();
    }
}
