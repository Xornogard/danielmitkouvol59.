﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour {

    public static PlanetGenerator instance;
    public GameObject planetPrefab;
    private const int MaximumPlanetsCount = 250;
    private const float fDelayBetweenPlanetsCreation = 0.25f;
    public UnityEngine.UI.Text planetsCountText;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }
    // Use this for initialization
    void Start () {
        StartCoroutine(CreatePlanets());
	}

    private void Update()
    {
        planetsCountText.text = "Planets Count: " +  PlanetControl.PlanetsCount.ToString();
    }

    public GameObject[] CreatePlanetsAtPoint(int iPlanetCounts, Vector3 createPosition)
    {
        List<GameObject> createdPlanetsList = new List<GameObject>();
        for (int i = 0; i < iPlanetCounts; i++)
        {
            createdPlanetsList.Add(CreatePlanetAtPoint(createPosition));
        }

        return createdPlanetsList.ToArray();
    }

    private GameObject CreatePlanetAtPoint(Vector3 createPosition)
    {
        return (GameObject) Instantiate(planetPrefab, createPosition, Quaternion.identity);
    }

    IEnumerator CreatePlanets()
    {
        while(true)
        {
            yield return new WaitForSeconds(fDelayBetweenPlanetsCreation);
            if (PlanetControl.PlanetsCount >= MaximumPlanetsCount) continue;

            Vector3 currentCreationPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0.0f, Screen.width), Random.Range(0.0f, Screen.height), 10.0f));
            var currentPlanet = CreatePlanetAtPoint(currentCreationPosition);
            currentPlanet.transform.parent = transform;
                
        }
    }



}
