﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetControl : MonoBehaviour {


    private const float GravitationalFactor = 0.0667f;
    private const float MassRadiusFactor = 0.1f;
    private const float StartMass = 1.0f;
    private const float MaximumMass = 50.0f;
    public Rigidbody rb;
    public static int PlanetsCount;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        PlanetsCount++;
        rb.mass = StartMass;
        transform.localScale = Vector3.one * rb.mass * MassRadiusFactor;
    }

    private void OnDestroy()
    {
        PlanetsCount--;
    }

    public void DeactivateCollision(float fDeactivationTime)
    {
        rb.detectCollisions = false;
        StartCoroutine(WaitForActivatingCollision(fDeactivationTime));
    }

    IEnumerator WaitForActivatingCollision(float fWaitTime)
    {
        yield return new WaitForSeconds(fWaitTime);
        rb.detectCollisions = true;
    }
	
	// Update is called once per frame
	void Update () {
        Collider[] planetsNearby = Physics.OverlapSphere(transform.position, transform.lossyScale.magnitude*2, 1 << LayerMask.NameToLayer("Planet"));

        foreach(Collider currentPlanet in planetsNearby)
        {
            var currentPlanetControl = currentPlanet.GetComponent<PlanetControl>();
            if (currentPlanetControl == this || currentPlanetControl.rb == null) continue;

            float fDistanceBetweenPlanets = Vector3.Distance(transform.position, currentPlanet.transform.position);
            float fGravitationalForce = GravitationalFactor * ((currentPlanetControl.rb.mass + rb.mass) / (fDistanceBetweenPlanets* fDistanceBetweenPlanets));

            Vector3 directionToPlanet = (transform.position - currentPlanet.transform.position).normalized;

            currentPlanetControl.rb.AddForce(directionToPlanet*fGravitationalForce);
        }
	}


    private void OnCollisionEnter(Collision collision)
    {
        float fCollidingMass = collision.collider.GetComponent<PlanetControl>().rb.mass;

        if(fCollidingMass > rb.mass)
        {
            Destroy(gameObject);
            return;
        }

        rb.mass += fCollidingMass;
        rb.velocity = Vector3.zero;
        transform.localScale = Vector3.one * rb.mass * MassRadiusFactor;

        if(rb.mass > MaximumMass)
            ExplodeIntoSmallerPlanets();

    }


    private void ExplodeIntoSmallerPlanets()
    {
        var planetsCreatedAfterExlosion = PlanetGenerator.instance.CreatePlanetsAtPoint( (int) (rb.mass / StartMass), transform.position);

        foreach(var currentPlanet in planetsCreatedAfterExlosion)
        {
            Vector3 currentPlanetDirection = new Vector3(Random.Range(-rb.mass * MassRadiusFactor, rb.mass * MassRadiusFactor), Random.Range(-rb.mass * MassRadiusFactor, rb.mass * MassRadiusFactor), Random.Range(-rb.mass * MassRadiusFactor, rb.mass * MassRadiusFactor));

            currentPlanet.transform.position += currentPlanetDirection;

            currentPlanetDirection.Normalize();

            var currentPlanetControl = currentPlanet.GetComponent<PlanetControl>();
            currentPlanetControl.rb.AddForce(currentPlanetDirection* Random.Range(1000.0f, 2000.0f));
            currentPlanetControl.DeactivateCollision(0.5f);
        }
        Destroy(gameObject);

    }
}
